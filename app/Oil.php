<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oil extends Model
{
    protected $fillable = [
        'usage', 'type', 'brand_id', 'max', 'min'
    ];

    public function brand() {
        return $this->belongsTo('App\Brand');
    }

    public function oilsIn() {
        return $this->hasMany('App\OilIn');
    }

    public function oilsOut() {
        return $this->hasMany('App\OilOut');
    }
}
