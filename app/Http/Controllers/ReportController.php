<?php

namespace App\Http\Controllers;

use App\Report;
use App\RefectionOut;
use App\Refection;
use App\Vehicle;
use App\Oil;
use App\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ReportController extends Controller
{
    public function detail(Vehicle $vehicle) 
    {
        return view('report.detail', compact('vehicle'));
    }

    public function vehicles()
    {
        $p = $this->getPermission('vehicle');
        $data = Vehicle::with(['Brand','Dependency'])->get();
        if(isset($p))
            foreach($data as $d)
                $d['p'] = array('a'=>false && $p->u,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    public function refections(Request $request)
    {
        $data = RefectionOut::with(['refection', 'refection.brand', 'vehicle'])->whereBetween('refection_outs.created_at', [
            Carbon::parse($request->input('start_date')), Carbon::parse($request->input('end_date'))
        ]);
        return datatables()->of($data)->toJson();
    }

    public function inventory(Request $request)
    {
        $data = Refection::with(['refectionsIn', 'refectionsOut', 'brand'])->get();
        foreach($data as $key => $value) {
            $in = 0;
            $out = 0;
            foreach ($value['refectionsIn'] as $k => $v)
                $in += $v->qty;
            foreach ($value['refectionsOut'] as $k => $v)
                $out += $v->qty;
            $data[$key]['total_in'] = $in;
            $data[$key]['total_out'] = $out;
            $data[$key]['total'] = $in - $out;
        }
        return datatables()->of($data)->toJson();
    }

    public function fluids(Request $request)
    {
        $data = Oil::with(['oilsIn', 'oilsOut', 'brand'])->get();
        foreach($data as $key => $value) {
            $in = 0;
            $out = 0;
            foreach ($value['oilsIn'] as $k => $v)
                $in += $v->qty;
            foreach ($value['oilsOut'] as $k => $v)
                $out += $v->qty;
            $data[$key]['total_in'] = $in;
            $data[$key]['total_out'] = $out;
            $data[$key]['total'] = $in - $out;
        }
        return datatables()->of($data)->toJson();
    }

    public function pending(Request $request)
    {
        $data = Vehicle::with(['vehicle_in', 'vehicle_out', 'brand'])->get();
        foreach($data as $key => $value) {
            $in = 0;
            $out = 0;
            foreach ($value['vehicle_in'] as $k => $v)
                $in++;
            foreach ($value['vehicle_out'] as $k => $v)
                $out++;
            if($in - $out <= 0)
                unset($data[$key]);
        }
        //return response()->json($data);
        return datatables()->of($data)->toJson();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('report.index', [ 'report'=>new Report() ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        return view('report.edit', compact('report'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        return (int)$report->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        return (int)$report->delete();
    }
}
