<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'created_at' => '2018-12-26 13:50:30',
                'icon' => NULL,
                'menu' => 1,
                'parent' => 3,
                'priority' => 1,
                'route' => 'user',
                'title' => 'Usuarios',
                'updated_at' => '2018-12-26 13:50:30',
            ),
            1 => 
            array (
                'created_at' => '2018-12-26 13:50:30',
                'icon' => NULL,
                'menu' => 1,
                'parent' => 4,
                'priority' => 1,
                'route' => 'permission',
                'title' => 'Permisos',
                'updated_at' => '2018-12-26 13:50:30',
            ),
            2 => 
            array (
                'created_at' => '2018-12-26 13:50:30',
                'icon' => 'flaticon-users',
                'menu' => 1,
                'parent' => -1,
                'priority' => 3,
                'route' => NULL,
                'title' => 'Usuarios y Dependencias',
                'updated_at' => '2019-06-13 12:26:47',
            ),
            3 => 
            array (
                'created_at' => '2018-12-26 13:50:30',
                'icon' => 'la la-code',
                'menu' => 1,
                'parent' => -1,
                'priority' => 4,
                'route' => NULL,
                'title' => 'Developers',
                'updated_at' => '2019-06-13 12:26:47',
            ),
            4 => 
            array (
                'created_at' => '2018-12-26 13:50:30',
                'icon' => NULL,
                'menu' => 1,
                'parent' => 4,
                'priority' => 2,
                'route' => 'role',
                'title' => 'Roles',
                'updated_at' => '2018-12-26 13:50:30',
            ),
            5 => 
            array (
                'created_at' => '2018-12-26 13:50:30',
                'icon' => NULL,
                'menu' => 0,
                'parent' => -1,
                'priority' => 2,
                'route' => NULL,
                'title' => 'Admin',
                'updated_at' => '2019-06-13 12:26:47',
            ),
            6 => 
            array (
                'created_at' => '2018-12-26 13:50:30',
                'icon' => NULL,
                'menu' => 0,
                'parent' => -11,
                'priority' => 6,
                'route' => 'developer',
                'title' => 'Developer',
                'updated_at' => '2018-12-26 13:50:30',
            ),
            7 => 
            array (
                'created_at' => '2019-05-15 20:21:06',
                'icon' => 'fa fa-car',
                'menu' => 1,
                'parent' => -1,
                'priority' => 1,
                'route' => 'noruta',
                'title' => 'Catalogos',
                'updated_at' => '2019-06-13 12:26:17',
            ),
            8 => 
            array (
                'created_at' => '2019-05-15 20:21:22',
                'icon' => 'la la-film',
                'menu' => 1,
                'parent' => 8,
                'priority' => 2,
                'route' => 'vehicle',
                'title' => 'Vehiculos',
                'updated_at' => '2019-06-13 12:26:47',
            ),
            9 => 
            array (
                'created_at' => '2019-05-27 10:48:08',
                'icon' => 'fa fa-toolbox',
                'menu' => 1,
                'parent' => 8,
                'priority' => 5,
                'route' => 'refection',
                'title' => 'Refacciones',
                'updated_at' => '2019-06-13 12:26:47',
            ),
            10 => 
            array (
                'created_at' => '2019-05-27 10:48:31',
                'icon' => 'fa fa-oil-can',
                'menu' => 1,
                'parent' => 8,
                'priority' => 8,
                'route' => 'oil',
                'title' => 'Aceites',
                'updated_at' => '2019-06-13 12:26:47',
            ),
            11 => 
            array (
                'created_at' => '2019-05-27 10:50:00',
                'icon' => 'la la-certificate',
                'menu' => 1,
                'parent' => 8,
                'priority' => 1,
                'route' => 'brand',
                'title' => 'Marcas',
                'updated_at' => '2019-06-13 12:26:47',
            ),
            12 => 
            array (
                'created_at' => '2019-06-05 12:16:49',
                'icon' => 'fa fa-car-crash',
                'menu' => 1,
                'parent' => 8,
                'priority' => 3,
                'route' => 'vehicleIn',
                'title' => 'Entrada de Vehiculos',
                'updated_at' => '2019-06-13 12:26:47',
            ),
            13 => 
            array (
                'created_at' => '2019-06-05 15:23:23',
                'icon' => 'fa fa-car-side',
                'menu' => 1,
                'parent' => 8,
                'priority' => 4,
                'route' => 'vehicleOut',
                'title' => 'Salida de Vehiculos',
                'updated_at' => '2019-06-13 12:26:47',
            ),
            14 => 
            array (
                'created_at' => '2019-06-06 13:37:22',
                'icon' => 'fa fa-toolbox',
                'menu' => 1,
                'parent' => 8,
                'priority' => 6,
                'route' => 'refectionIn',
                'title' => 'Entrada de Refacciones',
                'updated_at' => '2019-06-13 12:26:47',
            ),
            15 => 
            array (
                'created_at' => '2019-06-06 14:45:44',
                'icon' => 'flaticon-logout',
                'menu' => 1,
                'parent' => 8,
                'priority' => 7,
                'route' => 'refectionOut',
                'title' => 'Salida de Refacciones',
                'updated_at' => '2019-06-13 12:26:47',
            ),
            16 => 
            array (
                'created_at' => '2019-06-07 10:28:42',
                'icon' => 'fa fa-oil-can',
                'menu' => 1,
                'parent' => 8,
                'priority' => 9,
                'route' => 'oilIn',
                'title' => 'Entrada de Aceites',
                'updated_at' => '2019-06-13 12:26:47',
            ),
            17 => 
            array (
                'created_at' => '2019-06-07 12:49:20',
                'icon' => 'fa fa-oil-can',
                'menu' => 1,
                'parent' => 8,
                'priority' => 10,
                'route' => 'oilOut',
                'title' => 'Salida de Aceites',
                'updated_at' => '2019-06-13 12:26:47',
            ),
        ));
        
        
    }
}