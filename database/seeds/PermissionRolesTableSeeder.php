<?php

use Illuminate\Database\Seeder;

class PermissionRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission__roles')->delete();
        
        \DB::table('permission__roles')->insert(array (
            0 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 18,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            1 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 9,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            2 => 
            array (
                'admin' => 0,
                'c' => 0,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 0,
                'permission_id' => 8,
                'r' => 0,
                'role_id' => 1,
                'u' => 0,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            3 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 13,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            4 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 14,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            5 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 10,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            6 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 15,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            7 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 16,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            8 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 11,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            9 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 17,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            10 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 12,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            11 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 1,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            12 => 
            array (
                'admin' => 0,
                'c' => 0,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 0,
                'permission_id' => 3,
                'r' => 0,
                'role_id' => 1,
                'u' => 0,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            13 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 2,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            14 => 
            array (
                'admin' => 0,
                'c' => 0,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 0,
                'permission_id' => 4,
                'r' => 0,
                'role_id' => 1,
                'u' => 0,
                'updated_at' => '2019-06-07 12:49:36',
            ),
            15 => 
            array (
                'admin' => 1,
                'c' => 1,
                'created_at' => '2019-06-07 12:49:36',
                'd' => 1,
                'permission_id' => 5,
                'r' => 1,
                'role_id' => 1,
                'u' => 1,
                'updated_at' => '2019-06-07 12:49:36',
            ),
        ));
        
        
    }
}