<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'active' => 1,
                'created_at' => '2018-12-26 13:50:30',
                'description' => 'Todos los permisos',
                'name' => 'Developer',
                'state' => 'role',
                'updated_at' => '2018-12-26 13:50:30',
            ),
        ));
        
        
    }
}