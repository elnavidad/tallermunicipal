"use strict;"

var Reports = function() {
    var table;
    var start, end;
    // Configuracion de la validacion
    var config = {
        error: Common.eHandler,
        rules: {
            report: {
                required: true
            }
        }
    };

    function setEvents() {
        $('#type').select2({
            placeholder: "Seleccione tipo de reporte",
            width: "100%"
        });
        $('#type').on('change', reportChanged);

        picker = $('#m_dashboard_daterangepicker');
        var s = moment().startOf('month');
        var e = moment().endOf('month');



        picker.daterangepicker({
            direction: mUtil.isRTL(),
            startDate: s,
            endDate: e,
            buttonClasses: 'btn btn-sm',
            applyButtonClasses: 'btn-brand',
            locale: {
                customRangeLabel: "Rango Personalizado",
                cancelLabel: "Cancelar",
                applyLabel: "Aplicar"
            },
            opens: 'left',
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Ultimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Ultimos 30 dias': [moment().subtract(29, 'days'), moment()],
                'Este mes': [moment().startOf('month'), moment().endOf('month')],
                'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, dateChanged);

        dateChanged(s, e, '');

        /*config.url = '/admin/report';
        config.success = newReport;
        Common.validator($("#submitForm"), config);
        var porlet = new mPortlet('portlet_1');
        
        porlet.on('reload', function() {
            table.ajax.reload();
        });*/
    }

    function reportChanged() {
        switch ($(this).val()) {
            case "Vehiculo":
                RptVehiculo(start, end);
                break;
            case "Refacciones":
                RptRefacciones(start, end);
                break;
            case "Inventario":
                RptInventario(start, end);
                break;
            case "Aceites":
                RptAceites(start, end);
                break;
            case "Pendientes":
                RptPendientes(start, end);
                break;
            default:
                break;
        }
    }

    function RptVehiculo(start, end) {
        var coldefs = [{
                data: 'dependency.name',
                title: 'Dependencia'
            }, {
                data: 'economic_number',
                title: 'Número Económico'
            }, {
                data: 'plate',
                title: 'Número de Placas'
            }, {
                data: 'serial_number',
                title: 'Número de Serie'
            }, {
                data: 'brand.brand',
                title: 'Marca'
            }, {
                data: 'model',
                title: 'Modelo'
            }, {
                data: 'year',
                title: 'Año'
            }, {
                data: 'created_at',
                title: 'Creado',
                className: 'none',
                render: Common.dateFormat
            }, {
                data: 'updated_at',
                title: 'Actualizado',
                render: Common.dateFormat
            },
            {
                data: 'p',
                title: 'Acciones',
                className: 'all',
                render: function(d, t, r) {

                    return '<a data-container="body" data-id="' + r.id + '" data-action="detail" data-toggle="m-tooltip" data-placemente="top" data-title = "Detalle" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill">' +
                        '<i class="la la-eye"></i></a>';
                }
            }
        ];
        if (table != undefined) {
            table.destroy();
            $('#table').parent().html('<table class="table table-striped table-bordered table-hover" id="table"></table>');
        }
        table = Common.remoteTable($('#table'), '/admin/report/rpt_vehicles', coldefs);
        table.on('click', '[data-action="detail"]', goToDetail);
    }

    function goToDetail() {
        AppRouter.goToState('vehicle_detail', {
            id: $(this).data('id')
        });
    }

    function RptRefacciones(s, e) {
        var coldefs = [{
            data: 'refection.description',
            title: 'Refacción'
        }, {
            data: 'refection.brand.brand',
            title: 'Marca'
        }, {
            data: 'vehicle.economic_number',
            title: 'Vehículo'
        }, {
            data: 'reason',
            title: 'Detalles de instalación'
        }, {
            data: 'qty',
            title: 'Cantidad de piezas'
        }, {
            data: 'created_at',
            title: 'Creado',
            className: 'none',
            render: Common.dateFormat
        }, {
            data: 'updated_at',
            title: 'Actualizado',
            render: Common.dateFormat
        }];
        if (table != undefined) {
            table.destroy();
            $('#table').parent().html('<table class="table table-striped table-bordered table-hover" id="table"></table>');
        }
        table = Common.remoteTable($('#table'), {
            url: '/admin/report/rpt_refections',
            data: {
                start_date: function() {
                    return s;
                },
                end_date: function() {
                    return e;
                }
            }
        }, coldefs);
    }

    function RptInventario(start, end) {
        var coldefs = [{
            data: 'description',
            title: 'Refacción'
        }, {
            data: 'brand.brand',
            title: 'Marca'
        }, {
            data: 'model',
            title: 'Modelo'
        }, {
            data: 'total_in',
            title: 'Entradas'
        }, {
            data: 'total_out',
            title: 'Salidas'
        }, {
            data: 'total',
            title: 'En existencia'
        }, {
            data: 'created_at',
            title: 'Creado',
            className: 'none',
            render: Common.dateFormat
        }, {
            data: 'updated_at',
            title: 'Actualizado',
            render: Common.dateFormat
        }];
        if (table != undefined) {
            table.destroy();
            $('#table').parent().html('<table class="table table-striped table-bordered table-hover" id="table"></table>');
        }
        table = Common.remoteTable($('#table'), '/admin/report/rpt_inventory', coldefs);

    }

    function RptAceites(start, end) {
        var coldefs = [{
            data: 'usage',
            title: 'Fluidos para'
        }, {
            data: 'type',
            title: 'Categoría'
        }, {
            data: 'brand.brand',
            title: 'Marca'
        }, {
            data: 'total_in',
            title: 'Litros entrantes'
        }, {
            data: 'total_out',
            title: 'Litros salientes'
        }, {
            data: 'total',
            title: 'Litros en existencia'
        }, {
            data: 'created_at',
            title: 'Creado',
            className: 'none',
            render: Common.dateFormat
        }, {
            data: 'updated_at',
            title: 'Actualizado',
            render: Common.dateFormat
        }];
        if (table != undefined) {
            table.destroy();
            $('#table').parent().html('<table class="table table-striped table-bordered table-hover" id="table"></table>');
        }
        table = Common.remoteTable($('#table'), '/admin/report/rpt_fluids', coldefs);

    }

    function RptPendientes(start, end) {
        var coldefs = [{
            data: 'economic_number',
            title: 'Vehículo'
        }, {
            data: 'brand.brand',
            title: 'Marca'
        }, {
            data: 'model',
            title: 'Modelo'
        }, {
            data: 'vehicle_in',
            title: 'fecha de entrada',
            render: function(d) {
                return d[d.length - 1].created_at;
            }
        }, {
            data: 'vehicle_in',
            title: 'Motivo de entrada',
            render: function(d) {
                return d[d.length - 1].reason;
            }
        }];
        if (table != undefined) {
            table.destroy();
            $('#table').parent().html('<table class="table table-striped table-bordered table-hover" id="table"></table>');
        }
        table = Common.remoteTable($('#table'), '/admin/report/rpt_pending', coldefs);

    }

    function dateChanged(s, e, label) {
        var title = '';
        var range = '';

        if ((e - s) < 100 || label == 'Today') {
            title = 'Hoy:';
            range = s.format('MMM D');
        } else if (label == 'Ayer') {
            title = 'Ayer:';
            range = s.format('MMM D');
        } else {
            range = s.format('MMM D') + ' - ' + e.format('MMM D');
        }

        picker.find('.m-subheader__daterange-date').html(range);
        picker.find('.m-subheader__daterange-title').html(title);

        pickerDates = [s, e];
        start = s.format();
        end = e.format();
        $('#type').trigger('change');

    }

    function getReports() {
        var coldefs = [{
                data: 'created_at',
                title: 'Creado',
                className: 'none',
                render: Common.dateFormat
            }, {
                data: 'updated_at',
                title: 'Actualizado',
                render: Common.dateFormat
            },
            {
                data: 'p',
                title: 'Acciones',
                className: 'all',
                render: Common.tableActions
            }
        ];
        table = Common.remoteTable($('#table'), '/admin/report/datatable', coldefs);
    }



    function newReport(data) {
        Common.success("Atencion!", "Reporte generado con exito.");
        table.ajax.reload();
    }

    return {
        init: function() {
            setEvents();
            //getReports();
        }
    };
}();