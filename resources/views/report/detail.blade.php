<div class="row">
    @component('components.portlet', ['open'=>true])
    @slot('title')
    Detalle de Vehiculo
    @endslot
    @slot('tools')
    <a href="#/Reportes" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10">
        <span>
            <i class="la la-arrow-left"></i>
            <span>Regresar</span>
        </span>
    </a>
    @endslot
    <div class="m-widget12">
        <div class="m-widget12__item">
            <span class="m-widget12__text1">Numero Economico<br><span>{{ $vehicle->economic_number }}</span></span>
            <span class="m-widget12__text2">Marca<br><span>{{ $vehicle->brand->brand }}</span></span>
        </div>
        <div class="m-widget12__item">
            <span class="m-widget12__text1">Modelo<br><span>{{ $vehicle->model }}</span></span>
            <span class="m-widget12__text2">Año<br><span>{{ $vehicle->year }}</span></span>
        </div>
        <div class="m-widget12__item">
            <span class="m-widget12__text1">Numero de Serie<br><span>{{ $vehicle->serial_number }}</span></span>
            <span class="m-widget12__text2">Dependencia<br><span>{{ $vehicle->dependency->name }}</span></span>
        </div>
    </div>
    @endcomponent
    @component('components.portlet', ['open'=>true])
    @slot('title')
    Entradas
    @endslot
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>Razon de Entrada</th>
            <th>Fecha de Entrada</th>
        </thead>
        <tbody>
            @foreach ($vehicle->vehicle_in as $in)
            <tr>
                <td>{{  $in->reason }}</td>
                <td>{{  $in->created_at }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endcomponent
    @component('components.portlet', ['open'=>true])
    @slot('title')
    Salidas
    @endslot
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>Detalle de Salida</th>
            <th>Fue reparado</th>
            <th>Fecha de Salida</th>
        </thead>
        <tbody>
            @foreach ($vehicle->vehicle_out as $out)
            <tr>
                <td>{{  $out->reason }}</td>
                <td>{{  $out->is_fixed == 1 ? "Si" : "No" }}</td>
                <td>{{  $out->created_at }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endcomponent
    @component('components.portlet', ['open'=>true])
    @slot('title')
    Refacciones Utilizadas
    @endslot
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>Refaccion</th>
            <th>Razon de Salida</th>
            <th>Cantidad</th>
            <th>Fecha</th>
        </thead>
        <tbody>
            @foreach ($vehicle->refections as $refection)
            <tr>
                <td>{{  $refection->refection->description }}</td>
                <td>{{  $refection->reason }}</td>
                <td>{{  $refection->qty }}</td>
                <td>{{  $refection->created_at }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endcomponent
    @component('components.portlet', ['open'=>true])
    @slot('title')
    Fluidos Utilizados
    @endslot
    <table class="table table-striped table-bordered table-hover">
            <thead>
                <th>Fluido</th>
                <th>Razon de Salida</th>
                <th>Litros</th>
                <th>Fecha</th>
            </thead>
            <tbody>
                @foreach ($vehicle->oils as $oil)
                <tr>
                    <td>{{  $oil->oil->description }}</td>
                    <td>{{  $oil->reason }}</td>
                    <td>{{  $oil->qty }}</td>
                    <td>{{  $oil->created_at }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @endcomponent
</div>