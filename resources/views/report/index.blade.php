<div class="row">
    @if($permission->c)
        @component('components.portlet', [])
            @slot('title')
            Generar Reportes
            @endslot
            @include('report.form')
        @endcomponent
    @endif
    @if($permission->r)
        @component('components.portlet', ['table'=>'table'])
            @slot('title')
            Reportes
            @endslot
        @endcomponent
    @endif
</div>