<div class="row">

    <div class="col-sm-4">
        <div class="form-group m-form__group">
            <label for="">Tipo de Reporte</label>
            <div class="m-select2 m-select2--air">
                <select name="type" id="type" class="form-control m-select2">
                    <option value="Vehiculo">Vehículo</option>
                    <option value="Refacciones">Refacciones</option>
                    <option value="Inventario">Inventario</option>
                    <option value="Aceites">Aceites</option>
                    <option value="Pendientes">Pendientes</option>
                </select>
            </div>
        </div>

    </div>
    <div class="col-sm-3"></div>
    <div class="col-sm-3">
        <div class="form-group m-form__group">
            <label for="">Fecha</label>
            <div>
                <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                    <span class="m-subheader__daterange-label">
                        <span class="m-subheader__daterange-title"></span>
                        <span class="m-subheader__daterange-date m--font-brand"></span>
                    </span>
                    <a href="javascript:void(0);" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--square">
                        <i class="la la-angle-down"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>

</div>